#![warn(clippy::explicit_iter_loop)]
#![warn(clippy::flat_map_option)]
#![warn(clippy::if_then_some_else_none)]
#![warn(clippy::use_self)]

mod budget_chat;
mod means_to_an_end;
mod prime_time;
mod smoke_test;

use async_std::{
    net::{TcpListener, TcpStream},
    task::spawn,
};
use clap::{Parser, Subcommand};
use futures::StreamExt;
use uuid::Uuid;

#[derive(Parser)]
struct Opts {
    #[clap(short, long, default_value_t = 29999)]
    port: u16,
    #[clap(subcommand)]
    service: Service,
}

#[derive(Clone, Subcommand)]
enum Service {
    BudgetChat(budget_chat::BudgetChat),
    MeansToAnEnd,
    PrimeTime,
    SmokeTest,
}

impl Service {
    async fn handle(self, stream: TcpStream) {
        let id = Uuid::new_v4();
        log::debug!("Handling new client {id}");
        let f = spawn(async move {
            match self {
                Self::BudgetChat(chat) => chat.handle(stream).await,
                Self::MeansToAnEnd => means_to_an_end::handle(id, stream).await,
                Self::PrimeTime => prime_time::handle(id, stream).await,
                Self::SmokeTest => smoke_test::echo(id, stream).await,
            }
        });
        match f.await {
            Ok(_) => log::debug!("Successfully handled client {id}"),
            Err(e) => log::error!("Error handling client {id}: {e}"),
        }
    }
}

#[async_std::main]
async fn main() -> anyhow::Result<()> {
    env_logger::init();

    let opts = Opts::parse();

    let listener = TcpListener::bind(("0.0.0.0", opts.port)).await?;
    log::info!("Listening on port {}", listener.local_addr()?.port());

    listener
        .incoming()
        .for_each_concurrent(10, |stream| {
            let service = opts.service.clone();
            async move {
                match stream {
                    Ok(stream) => service.handle(stream).await,
                    Err(e) => log::error!("Error accepting client: {e}"),
                }
            }
        })
        .await;

    Ok(())
}
