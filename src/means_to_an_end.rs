use std::io::ErrorKind;

use async_std::net::TcpStream;
use futures::{AsyncReadExt, AsyncWriteExt};
use uuid::Uuid;

const HEADER_LENGTH: usize = 1;
const COMMAND_LENGTH: usize = 8;
const MESSAGE_LENGTH: usize = HEADER_LENGTH + COMMAND_LENGTH;

type Timestamp = i32;

#[derive(Debug)]
struct Asset {
    timestamp: Timestamp,
    price: i32,
}

impl From<&[u8; COMMAND_LENGTH]> for Asset {
    fn from(value: &[u8; COMMAND_LENGTH]) -> Self {
        Self {
            timestamp: Timestamp::from_be_bytes(value[0..COMMAND_LENGTH / 2].try_into().unwrap()),
            price: i32::from_be_bytes(
                value[COMMAND_LENGTH / 2..COMMAND_LENGTH]
                    .try_into()
                    .unwrap(),
            ),
        }
    }
}

#[derive(Debug)]
struct Query {
    min_time: Timestamp,
    max_time: Timestamp,
}

impl From<&[u8; COMMAND_LENGTH]> for Query {
    fn from(value: &[u8; COMMAND_LENGTH]) -> Self {
        Self {
            min_time: Timestamp::from_be_bytes(value[0..COMMAND_LENGTH / 2].try_into().unwrap()),
            max_time: Timestamp::from_be_bytes(
                value[COMMAND_LENGTH / 2..COMMAND_LENGTH]
                    .try_into()
                    .unwrap(),
            ),
        }
    }
}

pub(crate) async fn handle(id: Uuid, mut stream: TcpStream) -> anyhow::Result<()> {
    let mut assets = vec![];
    let mut buffer = [0_u8; MESSAGE_LENGTH];
    loop {
        if let Err(error) = stream.read_exact(&mut buffer).await {
            match error.kind() {
                ErrorKind::UnexpectedEof => {
                    log::info!("Connection closed for client {id}")
                }
                _ => log::error!("Connection shut down unexpectedly for client {id}"),
            }
            break;
        }
        log::trace!("Received {buffer:?} from client {id}");
        match buffer[0] {
            b'I' => {
                log::debug!("Received insert for client {id}");
                let asset = Asset::from(&buffer[1..MESSAGE_LENGTH].try_into().unwrap());
                log::debug!("Inserting asset {asset:?} for client {id}");
                assets.push(asset)
            }
            b'Q' => {
                log::debug!("Received query for client {id}");
                let query = Query::from(&buffer[1..MESSAGE_LENGTH].try_into().unwrap());
                log::debug!("Querying {query:?} for client {id}");
                let (sum, count) = assets
                    .iter()
                    .filter_map(|asset| {
                        (asset.timestamp >= query.min_time && asset.timestamp <= query.max_time)
                            .then_some(asset.price)
                    })
                    .fold((0_i128, 0), |(sum, count), price| {
                        (sum + price as i128, count + 1)
                    });
                let mean = if count == 0 { 0 } else { sum / count } as i32;
                log::debug!("Found {count} assets with sum {sum} and mean {mean} for client {id}");
                stream.write_all(&mean.to_be_bytes()).await?;
                log::debug!("Answered query with {mean} for client {id}");
            }
            invalid => {
                log::warn!("Invalid header {invalid} for client {id}");
                stream.close().await?;
                break;
            }
        }
    }
    Ok(())
}
