use async_std::{
    io::{ReadExt, WriteExt},
    net::TcpStream,
};
use uuid::Uuid;

pub(crate) async fn echo(id: Uuid, mut stream: TcpStream) -> anyhow::Result<()> {
    let mut buffer = vec![0; 512];
    let mut size;
    while {
        size = stream.read(&mut buffer).await?;
        size
    } > 0
    {
        log::debug!("Read {size} bytes for client {id}");
        stream.write_all(&buffer[0..size]).await?;
    }
    Ok(())
}
