use std::{net::Shutdown, sync::Arc};

use anyhow::anyhow;
use async_std::{
    io::{prelude::BufReadExt, BufReader, Read},
    net::TcpStream,
    sync::RwLock,
};
use clap::{ArgMatches, Args, Command, Error, FromArgMatches};
use futures::{future::try_join_all, AsyncWriteExt};
use indexmap::IndexMap;
use once_cell::sync::Lazy;
use regex::Regex;

const INVALID_USERNAME: &[u8] = "Invalid username\n".as_bytes();
const WELCOME: &[u8] = "Welcome to budgetchat! What shall I call you?\n".as_bytes();

static VALID_USERNAME: Lazy<Regex> = Lazy::new(|| Regex::new(r#"^[A-Za-z0-9]{1,16}$"#).unwrap());

#[derive(Clone, Default)]
pub(crate) struct BudgetChat {
    clients: Arc<RwLock<IndexMap<String, TcpStream>>>,
}

impl BudgetChat {
    pub(crate) async fn handle(&self, mut stream: TcpStream) -> anyhow::Result<()> {
        if let Ok(Some(user)) = self.welcome(&mut stream).await {
            while let Ok(Some(message)) = Self::get_string(&stream).await {
                let announce = format!("[{user}] {message}\n");
                try_join_all(
                    self.clients
                        .write()
                        .await
                        .iter_mut()
                        .filter(|(u, _)| u != &&user)
                        .map(|(_, w)| w.write_all(announce.as_bytes())),
                )
                .await?;
            }

            let announce = format!("* {user} has left the room\n");
            let mut clients = self.clients.write().await;
            clients.remove(&user);
            try_join_all(
                clients
                    .iter_mut()
                    .map(|(_, w)| w.write_all(announce.as_bytes())),
            )
            .await?;
        } else {
            stream.shutdown(Shutdown::Both)?;
        }

        Ok(())
    }

    async fn welcome(&self, stream: &mut TcpStream) -> anyhow::Result<Option<String>> {
        stream.write_all(WELCOME).await?;
        if let Some(user) = Self::get_string(stream.clone()).await? {
            if VALID_USERNAME.is_match(&user) {
                let mut clients = self.clients.write().await;
                let current = if clients.is_empty() {
                    "no one".to_string()
                } else {
                    clients.keys().cloned().collect::<Vec<_>>().join(", ")
                };
                stream
                    .write_all(format!("* The room contains: {}\n", current).as_bytes())
                    .await?;
                let announce = format!("* {user} has entered the room\n");
                try_join_all(
                    clients
                        .iter_mut()
                        .map(|(_, w)| w.write_all(announce.as_bytes())),
                )
                .await?;
                clients.insert(user.clone(), stream.clone());
                Ok(Some(user))
            } else {
                stream.write_all(INVALID_USERNAME).await?;
                Ok(None)
            }
        } else {
            Err(anyhow!("Did not get username"))
        }
    }

    async fn get_string<I: Read + Unpin>(input: I) -> anyhow::Result<Option<String>> {
        let mut reader = BufReader::new(input);
        let mut buffer = String::new();
        if reader.read_line(&mut buffer).await? > 0 {
            Ok(Some(buffer.trim().to_string()))
        } else {
            Ok(None)
        }
    }
}

impl Args for BudgetChat {
    fn augment_args(cmd: Command) -> Command {
        cmd
    }

    fn augment_args_for_update(cmd: Command) -> Command {
        cmd
    }
}

impl FromArgMatches for BudgetChat {
    fn from_arg_matches(_: &ArgMatches) -> Result<Self, Error> {
        Ok(Self::default())
    }

    fn update_from_arg_matches(&mut self, _: &ArgMatches) -> Result<(), Error> {
        Ok(())
    }
}
