use async_std::{
    io::{
        prelude::BufReadExt,
        {BufReader, WriteExt},
    },
    net::TcpStream,
};
use primes::is_prime;
use serde::{Deserialize, Serialize};
use serde_json::Number;
use uuid::Uuid;

#[derive(Deserialize)]
struct Request {
    method: Method,
    number: Number,
}

#[derive(Serialize)]
struct Response {
    method: Method,
    prime: bool,
}

#[derive(Deserialize, Serialize)]
enum Method {
    #[serde(rename = "isPrime")]
    IsPrime,
}

pub(crate) async fn handle(id: Uuid, mut stream: TcpStream) -> anyhow::Result<()> {
    let mut buffer = String::new();
    let mut reader = BufReader::new(stream.clone());
    while reader.read_line(&mut buffer).await? > 0 {
        let raw = buffer.trim();
        log::debug!("Received request for client {id}: {raw}");
        let response = match serde_json::from_str::<Request>(raw) {
            Ok(request) => {
                let prime = request.number.as_u64().map(is_prime).unwrap_or(false);
                serde_json::to_string(&Response {
                    method: request.method,
                    prime,
                })?
            }
            Err(e) => {
                let message = format!("Invalid request \"{raw}\": {e}");
                log::error!("{message}");
                message
            }
        };
        stream
            .write_all(format!("{}\n", response).as_bytes())
            .await?;
        buffer.clear();
    }
    Ok(())
}
